package net.ranura.marvelheros.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ranura.marvelheros.data.MarvelRepo

class CharacterListViewModelFactory(
    private val repository: MarvelRepo
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharacterListViewModel(repository) as T
    }
}