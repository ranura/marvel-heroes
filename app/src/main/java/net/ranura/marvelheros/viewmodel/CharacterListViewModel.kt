package net.ranura.marvelheros.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import net.ranura.marvelheros.data.Character
import net.ranura.marvelheros.data.CharacterListDataSourceFactory
import net.ranura.marvelheros.data.MarvelRepo

class CharacterListViewModel(repo: MarvelRepo) : ViewModel() {

    var loading = MutableLiveData<Boolean>(true)
    val characters: MediatorLiveData<PagedList<Character>> = MediatorLiveData()

    init {
        characters.addSource(repo.getCharacters()) {
            loading.postValue(false)
            characters.postValue(it)
        }
    }
}
