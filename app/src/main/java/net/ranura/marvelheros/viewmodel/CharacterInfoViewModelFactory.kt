package net.ranura.marvelheros.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ranura.marvelheros.data.MarvelRepo

class CharacterInfoViewModelFactory(
    private val repository: MarvelRepo,
    private val characterId: String?
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharacterInfoViewModel(repository, characterId) as T
    }
}