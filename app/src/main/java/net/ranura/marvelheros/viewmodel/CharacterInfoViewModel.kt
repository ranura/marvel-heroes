package net.ranura.marvelheros.viewmodel

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import net.ranura.marvelheros.data.Character
import net.ranura.marvelheros.data.MarvelRepo

class CharacterInfoViewModel(repo: MarvelRepo,characterId: String?) : ViewModel() {

    var loading = MutableLiveData<Boolean>(true)
    val currentCharacter: MediatorLiveData<Character?> = MediatorLiveData()

    init {
        currentCharacter.addSource(repo.getCharacterByCharacterId(characterId)) {
            loading.postValue(false)
            currentCharacter.postValue(it?.data?.results?.getOrNull(0))
        }
    }
}
