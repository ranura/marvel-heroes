package net.ranura.marvelheros.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import net.ranura.marvelheros.adapter.CharacterListAdapter
import net.ranura.marvelheros.data.MarvelRepo
import net.ranura.marvelheros.databinding.CharactersListFragmentBinding
import net.ranura.marvelheros.viewmodel.CharacterListViewModel
import net.ranura.marvelheros.viewmodel.CharacterListViewModelFactory


class CharacterListFragment : Fragment() {

    private val viewModel: CharacterListViewModel by viewModels {
        CharacterListViewModelFactory(MarvelRepo(requireContext().applicationContext))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = CharactersListFragmentBinding.inflate(inflater, container, false)
        context ?: return binding.root

        val characterListAdapter = CharacterListAdapter()
        binding.lifecycleOwner = viewLifecycleOwner
        binding.model = viewModel
        binding.characterListRecyclerview.layoutManager = GridLayoutManager(requireContext(), 2)
        binding.characterListRecyclerview.adapter = characterListAdapter

        viewModel.characters.observe(viewLifecycleOwner) { characters ->
            characterListAdapter.submitList(characters)
        }

        return binding.root
    }
}
