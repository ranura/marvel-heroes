package net.ranura.marvelheros.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import net.ranura.marvelheros.data.MarvelRepo
import net.ranura.marvelheros.databinding.CharactersInfoFragmentBinding
import net.ranura.marvelheros.viewmodel.CharacterInfoViewModel
import net.ranura.marvelheros.viewmodel.CharacterInfoViewModelFactory

class CharacterInfoFragment : Fragment() {
    val args: CharacterInfoFragmentArgs by navArgs()

    private val viewModel: CharacterInfoViewModel by viewModels {
        CharacterInfoViewModelFactory(
            MarvelRepo(requireContext().applicationContext),
            args.characterId
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = CharactersInfoFragmentBinding.inflate(inflater, container, false)
        context ?: return binding.root
        binding.lifecycleOwner = viewLifecycleOwner
        binding.model = viewModel

        return binding.root
    }
}
