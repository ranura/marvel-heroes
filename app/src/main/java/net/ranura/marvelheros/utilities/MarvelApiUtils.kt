package net.ranura.marvelheros.utilities

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response


class HeaderInterceptor : Interceptor {
    val headerReferer = "ranura.net"
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("referer", headerReferer)
                .url(updateOriginalUrl(request().url()))
                .build()
        )
    }


    private fun updateOriginalUrl(httpUrl: HttpUrl): HttpUrl {
        return httpUrl.newBuilder().addQueryParameter("apikey", "32493b46261eb6c90eed1e76374aefa6").build()
    }
}

