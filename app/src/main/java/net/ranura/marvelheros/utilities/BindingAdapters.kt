package net.ranura.marvelheros.utilities

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import net.ranura.marvelheros.data.Image

@BindingAdapter("thumbnail")
fun loadImage(imageView: ImageView, thumbnail: Image?) {
    thumbnail?.let {
        val path = thumbnail.path?.replace("http://", "https://")
        Glide.with(imageView.context)
            .load(path + "/standard_fantastic." + thumbnail.extension)
            .into(imageView)
    }
}