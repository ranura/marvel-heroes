package net.ranura.marvelheros.utilities

import android.text.Spanned
import androidx.core.text.HtmlCompat

object StringUtil {
    @JvmStatic
    fun fromHtml(htmlString: String?): Spanned {
        return HtmlCompat.fromHtml(htmlString.orEmpty(), HtmlCompat.FROM_HTML_MODE_LEGACY)
    }
}