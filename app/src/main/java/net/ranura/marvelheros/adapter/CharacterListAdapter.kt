package net.ranura.marvelheros.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import net.ranura.marvelheros.data.Character
import net.ranura.marvelheros.data.Image
import net.ranura.marvelheros.databinding.CharacterListItemBinding
import net.ranura.marvelheros.ui.main.CharacterListFragmentDirections

class CharacterListAdapter :
    PagedListAdapter<Character, RecyclerView.ViewHolder>(
        CharacterListCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CharacterViewHolder(
            CharacterListItemBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val character = getItem(position) ?: return
        (holder as CharacterViewHolder).bind(character)
    }

    class CharacterViewHolder(
        private val binding: CharacterListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Character) {
            binding.apply {
                character = item
                setClickListener {
                    val action =
                        CharacterListFragmentDirections.actionCharacterListFragmentToCharacterInfoFragment(
                            characterId = character?.id
                        )
                    it.findNavController().navigate(action)
                }
                executePendingBindings()
            }
        }
    }
}

private class CharacterListCallback : DiffUtil.ItemCallback<Character>() {

    override fun areItemsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Character, newItem: Character): Boolean {
        return oldItem == newItem
    }
}