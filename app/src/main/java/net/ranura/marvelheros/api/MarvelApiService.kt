package net.ranura.marvelheros.api

import android.content.Context
import net.ranura.marvelheros.data.CharacterDataWrapper
import net.ranura.marvelheros.utilities.HeaderInterceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface MarvelApiService {

    @GET("/v1/public/characters")
    fun getCharacters(@Query("limit") limit: Int, @Query("offset") offset: Int): Call<CharacterDataWrapper>

    @GET("/v1/public/characters/{characterId}")
    fun getCharacterById(@Path("characterId") characterId: String): Call<CharacterDataWrapper?>

    companion object {
        private const val READ_TIME_OUT = 20L
        private const val CONNECT_TIME_OUT = 20L
        const val baseUrl = "https://gateway.marvel.com"
        private var okHttpClient: OkHttpClient? = null
        fun create(context: Context): MarvelApiService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(
                    GsonConverterFactory.create()
                )
                .client(buildOkHttpClient(context))
                .baseUrl(baseUrl)
                .build()

            return retrofit.create(MarvelApiService::class.java)
        }

        private fun buildOkHttpClient(context: Context): OkHttpClient {
            return okHttpClient ?: OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .addInterceptor(HeaderInterceptor())
                .build()
        }
    }
}

