package net.ranura.marvelheros

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.main_activity.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val navController = findNavController(R.id.nav_host_fragment)
        toolbar.setLogo(R.mipmap.ic_launcher)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            if (destination.id == R.id.characterInfoFragment) {
                toolbar.logo = null
            } else {
                toolbar.setLogo(R.mipmap.ic_launcher)
            }
        }
        toolbar.setupWithNavController(navController)
    }

}
