package net.ranura.marvelheros.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PositionalDataSource
import net.ranura.marvelheros.api.MarvelApiService

class CharacterListDataSource(context: Context) : PositionalDataSource<Character>() {
    private val marvelApiService: MarvelApiService by lazy {
        MarvelApiService.create(context)
    }


    override fun loadInitial(
        params: LoadInitialParams,
        callback: LoadInitialCallback<Character>
    ) {
        val result =
            marvelApiService.getCharacters(
                params.requestedLoadSize,
                params.requestedStartPosition
            )
                .execute()

        val total = result.body()?.data?.total ?: 0
        result.body()?.data?.results?.let {
            callback.onResult(it, params.requestedStartPosition, total)
        }
    }

    override fun loadRange(
        params: LoadRangeParams,
        callback: LoadRangeCallback<Character>
    ) {
        val result =
            marvelApiService.getCharacters(params.loadSize, params.startPosition).execute()

        result.body()?.data?.results?.let {
            callback.onResult(it)
        }
    }

}