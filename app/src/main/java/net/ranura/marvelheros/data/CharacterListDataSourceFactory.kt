package net.ranura.marvelheros.data

import android.content.Context
import androidx.paging.DataSource


class CharacterListDataSourceFactory(val context: Context) : DataSource.Factory<Int, Character>() {
    override fun create(): DataSource<Int, Character> {
        return CharacterListDataSource(context)
    }
}