package net.ranura.marvelheros.data

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import net.ranura.marvelheros.api.MarvelApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MarvelRepo(val context: Context) {

    private val pagedListConfig: PagedList.Config =
        PagedList.Config.Builder()
            .setEnablePlaceholders(true)
            .setPrefetchDistance(5)
            .setInitialLoadSizeHint(5).setEnablePlaceholders(true)
            .setPageSize(20).build()
    private val sourceFactory = CharacterListDataSourceFactory(context)

    fun getCharacters() = LivePagedListBuilder(sourceFactory, pagedListConfig).build()

    fun getCharacterByCharacterId(characterId: String?): LiveData<CharacterDataWrapper?> {
        val result = MutableLiveData<CharacterDataWrapper?>()

        characterId?.let {
            MarvelApiService.create(context).getCharacterById(it)
                .enqueue(object : Callback<CharacterDataWrapper?> {
                    override fun onFailure(call: Call<CharacterDataWrapper?>, t: Throwable) {
                        result.postValue(null)
                    }

                    override fun onResponse(
                        call: Call<CharacterDataWrapper?>,
                        response: Response<CharacterDataWrapper?>
                    ) {
                        result.postValue(response.body())
                    }
                })
        } ?: run {
            result.postValue(null)
        }

        return result
    }
}