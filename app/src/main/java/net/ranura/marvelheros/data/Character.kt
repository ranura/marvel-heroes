package net.ranura.marvelheros.data

data class Character(
    val id: String?,
    val name: String,
    val description: String?,
    val thumbnail: Image?,
    val comics: ComicList?
)

data class Image(val path: String?, val extension: String?)

data class ComicList(val items: List<ComicSummary>) {
    fun toHtml(): String {
        return items.joinToString("<br>") {
            it.name.orEmpty()
        }
    }
}

data class ComicSummary(val name: String?)

data class CharacterDataContainer(
    val results: List<Character>?,
    val limit: Int,
    val total: Int,
    val count: Int
)

data class CharacterDataWrapper(val data: CharacterDataContainer)